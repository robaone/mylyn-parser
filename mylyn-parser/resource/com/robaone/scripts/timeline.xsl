<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<timeline>
			<xsl:for-each select="//notes">
				<xsl:sort select="startDate"/>
				<event>
					<startDate>
						<xsl:value-of select="startDate"/>
					</startDate>
					<endDate>
						<xsl:value-of select="endDate"/>
					</endDate>
					<project>
						<xsl:value-of select="./ancestor::project/name"/>
					</project>
					<task>
						<xsl:value-of select="./ancestor::tasks/name"/>
					</task>
					<note>
						<xsl:value-of select="body"/>
					</note>
				</event>
			</xsl:for-each>
		</timeline>
	</xsl:template>
</xsl:stylesheet>