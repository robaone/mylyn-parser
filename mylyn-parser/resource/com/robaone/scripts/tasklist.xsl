<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <tasks>
      <xsl:for-each select="/projects/project/tasks[status != 'Done']">
      	<xsl:sort select="duedate"/>
        <task>
          <name>
            <xsl:value-of select="name"/>
          </name>
          <duedate>
            <xsl:value-of select="duedate"/>
          </duedate>
          <remaining_hours>
            <xsl:variable name="estimated" select="estimatedhours"/>
            <xsl:variable name="actualhours" select="sum(notes/hours)"/>
            <xsl:call-template name="round">
              <xsl:with-param name="number" select="$estimated - $actualhours"/>
              <xsl:with-param name="places" select="0.25"/>
            </xsl:call-template>
          </remaining_hours>
          <href>
          	<xsl:value-of select="href"/>
          </href>
          <actualhours>
          	<xsl:call-template name="round">
          		<xsl:with-param name="number" select="sum(notes/hours)"/>
          		<xsl:with-param name="places" select="0.25"/>
          	</xsl:call-template>
          </actualhours>
        </task>
      </xsl:for-each>
    </tasks>
  </xsl:template>
  <xsl:template name="round">
		<xsl:param name="number"/>
		<xsl:param name="places"/>
		<xsl:variable name="remainder" select="$number mod $places"/>
		<xsl:variable name="truncated" select="$number - $remainder"/>
		<xsl:choose>
			<xsl:when test="$remainder &gt; 0">
				<xsl:value-of select="$truncated + $places"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$truncated"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>