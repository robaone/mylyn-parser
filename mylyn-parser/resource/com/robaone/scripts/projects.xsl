﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
	xmlns:xalan="http://xml.apache.org/xalan"
	xmlns:java="org.markdown4j" extension-element-prefixes="java">
	<xsl:output method="html" omit-xml-declaration="yes" />
	<xsl:template match="/">
		<html>
			<head>
				<title>Mylyn Projects</title>
				<link href="project.css" rel="stylesheet" type="text/css"/>
			</head>
			<body>
				<xsl:for-each select="/projects/project">
					<div class="report">
						<xsl:attribute name="id">
							<xsl:text>P</xsl:text>
							<xsl:value-of select="identifier"/>
						</xsl:attribute>
						<h1>
							<xsl:value-of select="name" />
						</h1>
						<table class="summary">
							<thead>
								<tr>
									<th>Task ID</th>
									<th>Description</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<xsl:for-each select="tasks">
									<tr>
										<td>
											<xsl:value-of select="position()"></xsl:value-of>
										</td>
										<td>
											<xsl:value-of select="name" />
											<br/>
											<span class="due">
												<xsl:text>Due </xsl:text> 
												<xsl:call-template name="formatDate">
													<xsl:with-param name="date" select="duedate"/>
												</xsl:call-template>
											</span>
										</td>
										<td>
											<xsl:value-of select="status" />
										</td>
									</tr>
								</xsl:for-each>
							</tbody>
						</table>
						<table class="estimates">
							<tbody>
								<tr>
									<td>Assigned To</td>
									<td>
										<xsl:value-of select="owner" />
									</td>
								</tr>
								<tr>
									<td>Estimated Hours</td>
									<td>
										<xsl:value-of select="sum(tasks/estimatedhours)" />
									</td>
								</tr>
								<tr>
									<td>Total hours</td>
									<td>
										<xsl:call-template name="round">
											<xsl:with-param name="number" select="sum(tasks/notes/hours)" />
											<xsl:with-param name="places" select="0.25"/>
										</xsl:call-template>
									</td>
								</tr>
								<tr>
									<td>Start Date</td>
									<td>
										<xsl:call-template name="formatDate">
											<xsl:with-param name="date" select="tasks/notes/startDate[not(. > ../../tasks/notes/startDate)][1]" />
										</xsl:call-template>
									</td>
								</tr>
							</tbody>
						</table>
						<xsl:for-each select="tasks">
							<h2>
								<xsl:if test="status='Done'">
									<img class="complete" alt="Complete" src="http://www.mrinaupdates.com/assets/img/p.png"/>
								</xsl:if>
								<span class="taskname">
									<xsl:value-of select="name" />
								</span>
							</h2>
							<table class="hours">
								<tbody>
									<tr>
										<td>Estimated Hours</td>
										<td>
											<xsl:value-of select="estimatedhours"/>
										</td>
										<td>Total Hours</td>
										<td>
											<xsl:call-template name="round">
												<xsl:with-param name="number" select="sum(notes/hours)"/>
												<xsl:with-param name="places" select="0.25"/>
											</xsl:call-template>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="task">
								<thead>
									<tr>
										<th>Date</th>
										<th>Details</th>
										<th>Hours</th>
									</tr>
								</thead>
								<tbody>
									<xsl:for-each select="notes">
										<tr>
											<td>
												<xsl:call-template name="formatDate">
													<xsl:with-param name="date" select="startDate" />
												</xsl:call-template>
											</td>
											<td>
												<div>
													<xsl:call-template name="formatTime">
														<xsl:with-param name="date" select="startDate" />
													</xsl:call-template>
													<xsl:text> - </xsl:text>
													<xsl:call-template name="formatTime">
														<xsl:with-param name="date" select="endDate" />
													</xsl:call-template>
												</div>
												<div class="cropped">
													<xsl:call-template name="markdown">
														<xsl:with-param name="text" select="body" />
													</xsl:call-template>
												</div>
											</td>
											<td>
												<xsl:call-template name="round">
													<xsl:with-param name="number" select="hours"/>
													<xsl:with-param name="places" select="0.25"/>
												</xsl:call-template>
											</td>
										</tr>
									</xsl:for-each>
								</tbody>
							</table>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="formatDate">
		<xsl:param name="date" />
		<xsl:value-of select="substring($date,6,2)" />
		<xsl:text>/</xsl:text>
		<xsl:value-of select="substring($date,9,2)" />
		<xsl:text>/</xsl:text>
		<xsl:value-of select="substring($date,1,4)" />
	</xsl:template>
	<xsl:template name="formatTime">
		<xsl:param name="date"/>
		<xsl:variable name="hour" select="substring($date,12,2)"/>
		<xsl:variable name="minute" select="substring($date,15,2)"/>
		<xsl:choose>
			<xsl:when test="$hour = 12">
				<xsl:value-of select="$hour"/>
				<xsl:text>:</xsl:text>
				<xsl:value-of select="$minute"/>
				<xsl:text>pm</xsl:text>
			</xsl:when>
			<xsl:when test="$hour > 12">
				<xsl:value-of select="$hour - 12"/>
				<xsl:text>:</xsl:text>
				<xsl:value-of select="$minute"/>
				<xsl:text>pm</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="number($hour)"/>
				<xsl:text>:</xsl:text>
				<xsl:value-of select="$minute"/>
				<xsl:text>am</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="round">
		<xsl:param name="number"/>
		<xsl:param name="places"/>
		<xsl:variable name="remainder" select="$number mod $places"/>
		<xsl:variable name="truncated" select="$number - $remainder"/>
		<xsl:choose>
			<xsl:when test="$remainder &gt; 0">
				<xsl:value-of select="$truncated + $places"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$truncated"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="markdown">
		<xsl:param name="text"/>
		<xsl:variable name="markdownProcessor" select="java:Markdown4jProcessor.new()"/>
		<xsl:value-of disable-output-escaping="yes" select="java:process($markdownProcessor,$text)"/>
	</xsl:template>
</xsl:stylesheet>
