<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" omit-xml-declaration="yes" 
	  />
	<xsl:template match="/">
	<!-- 
		<xsl:for-each select="/LAKER/*">
			<xsl:if test="position() &gt; 1">
				<xsl:text>,</xsl:text>
			</xsl:if>
			<xsl:value-of select="name()"/>
		</xsl:for-each>
		<xsl:text>
</xsl:text> -->
  <xsl:for-each select="/tasks/task">
  	<xsl:for-each select="*">
			<xsl:variable name="field" select="name()"/>
			<xsl:if test="position() &gt; 1">
				<xsl:text>,</xsl:text>
			</xsl:if>
			<xsl:text>"</xsl:text>
			<!-- <xsl:value-of select="$field"/> -->
			<xsl:value-of select="normalize-space(translate(text(),'&quot;',''))"/>
			<xsl:text>"</xsl:text>
		</xsl:for-each>
		<xsl:text>
</xsl:text>
  </xsl:for-each>
	</xsl:template>
</xsl:stylesheet>