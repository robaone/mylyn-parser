package com.robaone.mylyn.trello;

import static org.junit.Assert.*;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class TrelloAttachFileTest {
	TrelloAttachFile attach;
	TrelloCredentials credentials;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		attach = new TrelloAttachFile();
		attach.setUrl("http://localhost/fileupload.php");
		credentials = new TrelloCredentials();
		credentials.setKey("key");
		credentials.setToken("token");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRun() throws Exception {
		String filename = "/home/ansel/Public/test.txt";
		attach.setFilename(filename);
		attach.setName("test.txt");
		attach.setCardid("cardid");
		attach.setCredentials(credentials);
		attach.run();
		System.out.println(IOUtils.toString(attach.getResponse().getEntity().getContent()));
		if(attach.getException() != null){
			throw attach.getException();
		}
	}

}
