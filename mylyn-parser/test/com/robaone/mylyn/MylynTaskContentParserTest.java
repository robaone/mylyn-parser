package com.robaone.mylyn;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.markdown4j.Markdown4jProcessor;

import com.robaone.api.data.AppDatabase;

public class MylynTaskContentParserTest {
	private final class TestTask implements TaskInterface {
		@Override
		public void setIdentifier(String id) {
			
		}

		@Override
		public String getIdentifier() {
			return null;
		}

		@Override
		public void setOwner(String author) {
			
		}

		@Override
		public String getOwner() {
			return null;
		}

		@Override
		public void setName(String title) {
			
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public void setProjectId(String projectId) {
			
		}

		@Override
		public String getProjectId() {
			return null;
		}

		@Override
		public TaskNoteInterface newNote() {
			MylynNote note = new MylynNote();
			return note;
		}

		@Override
		public void save(TaskNoteInterface currentNote) throws Exception {
			
		}

		@Override
		public void setTarget(StorageTargetInterface target) {
			
		}

		@Override
		public int getNoteCount() throws Exception {
			return 0;
		}

		@Override
		public TaskNoteInterface getNote(int index) throws Exception {
			return null;
		}
	}

	private HashMap<String,ArrayList<TaskNoteInterface>> notes = new HashMap<String,ArrayList<TaskNoteInterface>>();
	private ProjectInterface project;
	@Before
	public void setUp() throws Exception {
		System.setProperty("debug", "true");
	}

	@Test
	public void testHeaderMatching1() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		String test = "1/1/13:";
		boolean matches = parser.isSectionHeader(test);
		assertTrue(matches);
	}
	@Test
	public void testHeaderMatching2() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		String test = "12/1/13:";
		boolean matches = parser.isSectionHeader(test);
		assertTrue(matches);
	}
	@Test
	public void testHeaderMatching3() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		String test = "12/31/13:";
		boolean matches = parser.isSectionHeader(test);
		assertTrue(matches);
	}
	@Test
	public void testHeaderMatching4() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		String test = "12/31/2013:";
		boolean matches = parser.isSectionHeader(test);
		assertTrue(matches);
	}
	@Test
	public void testHeaderMatching5() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		String test = "1/1/13: 6:00pm - 6:30pm";
		boolean matches = parser.isSectionHeader(test);
		assertTrue(matches);
	}
	@Test
	public void testTimePattern() throws Exception {
		String patternStr = "[ ]*([0-9][0-9]?[:][0-9]{2}[ ]?[AaPp][Mm]?)([ ]*[-][ ]*([0-9][0-9]?[:][0-9]{2}[ ]?[AaPp][Mm]?))?[ ]*";
		Pattern p = Pattern.compile(patternStr);
		String text = "6:00pm";
		Matcher m = p.matcher(text);
		assertTrue(m.matches());
		String text2 = "6:00a - 6:30pm";
		m = p.matcher(text2);
		assertTrue(m.matches());
	}
	@Test
	public void testTimeSpan1() throws Exception {
		String header = "1/1/2014: 11:00pm - 1:00am";
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setTask(new TestTask());
		assertTrue(parser.isSectionHeader(header));
		TaskNoteInterface note = parser.newSection(header);
		AppDatabase.writeLog(note.getStartDate().toString());
		AppDatabase.writeLog(note.getEndDate().toString());
		assertTrue(note.getEndDate().getTime() - note.getStartDate().getTime() == 1000*60*60*2);
	}
	@Test
	public void testTimeSpan2() throws Exception {
		String header = "1/1/2014: 11:00pm - 12:00am";
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setTask(new TestTask());
		assertTrue(parser.isSectionHeader(header));
		TaskNoteInterface note = parser.newSection(header);
		AppDatabase.writeLog(note.getStartDate().toString());
		AppDatabase.writeLog(note.getEndDate().toString());
		assertTrue(note.getEndDate().getTime() - note.getStartDate().getTime() == 1000*60*60);
	}
	@Test
	public void testTimeSpan3() throws Exception {
		String header = "1/1/2014: 8:00am - 10:00am";
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setTask(new TestTask());
		assertTrue(parser.isSectionHeader(header));
		TaskNoteInterface note = parser.newSection(header);
		AppDatabase.writeLog(note.getStartDate().toString());
		AppDatabase.writeLog(note.getEndDate().toString());
		assertTrue(note.getEndDate().getTime() - note.getStartDate().getTime() == 1000*60*60*2);
	}
	@Test
	public void testTimeSpan4() throws Exception {
		String header = "1/1/2014: 8:00am";
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setTask(new TestTask());
		assertTrue(parser.isSectionHeader(header));
		TaskNoteInterface note = parser.newSection(header);
		AppDatabase.writeLog(note.getStartDate().toString());
		AppDatabase.writeLog(note.getEndDate().toString());
		assertTrue(note.getEndDate().getTime() - note.getStartDate().getTime() == 0);
	}
	@Test
	public void testTimeSpan5() throws Exception {
		String header = "1/1/2014:";
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setTask(new TestTask());
		assertTrue(parser.isSectionHeader(header));
		TaskNoteInterface note = parser.newSection(header);
		AppDatabase.writeLog(note.getStartDate().toString());
		AppDatabase.writeLog(note.getEndDate().toString());
		assertTrue(note.getEndDate().getTime() - note.getStartDate().getTime() == 0);
	}
	@Test
	public void testRun() throws Exception {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setSource(this.getSource());
		parser.setTask(this.getTask());
		parser.run();
		TaskInterface output = parser.getTask();
		assertNotNull(output);
		assertEquals(2,output.getNoteCount());
		for(int index = 0; index < output.getNoteCount();index++){
			assertNotNull(output.getNote(index));
			TaskNoteInterface note = output.getNote(index);
			assertNotNull(note.getStartDate());
			assertNotNull(note.getEndDate());
			assertNotNull(note.getBody());
		}
	}
	private TaskInterface getTask() {
		TaskInterface task = new MylynTask();
		task.setTarget(getStorageTarget());
		task.setIdentifier("1");
		task.setProjectId("100000-000001");
		task.setName("Sample Task");
		task.setOwner("junit");
		return task;
	}

	private StorageTargetInterface getStorageTarget() {
		StorageTargetInterface target = new StorageTargetInterface(){

			@Override
			public void save(TaskNoteInterface currentNote,
					TaskInterface mylynStorage) {
				addNote(currentNote,mylynStorage);
			}

			@Override
			public int getNoteCount(TaskInterface mylynTask) throws Exception {
				return MylynTaskContentParserTest.this.notes.get(mylynTask.getProjectId()).size();
			}

			@Override
			public TaskNoteInterface getNote(int index, TaskInterface mylynTask)
					throws Exception {
				return MylynTaskContentParserTest.this.notes.get(mylynTask.getProjectId()).get(index);
			}

			@Override
			public void save(ProjectInterface project) {
				setProject(project);
			}
			
		};
		return target;
	}
	
	protected void setProject(ProjectInterface project){
		this.project = project;
	}

	protected void addNote(TaskNoteInterface currentNote, TaskInterface mylynStorage) {
		ArrayList<TaskNoteInterface> list = this.notes.get(mylynStorage.getProjectId());
		if(list == null){
			list = new ArrayList<TaskNoteInterface>();
			this.notes.put(mylynStorage.getProjectId(), list);
		}
		list.add(currentNote);
	}

	@Test
	public void testMarkdown() throws Exception {
		String html = new Markdown4jProcessor().process(this.getSource());
		AppDatabase.writeLog(html);
	}

	private String getSource() throws IOException {
		String str = IOUtils.toString(this.getClass().getResourceAsStream("markdown.txt"));
		return str;
	}

}
