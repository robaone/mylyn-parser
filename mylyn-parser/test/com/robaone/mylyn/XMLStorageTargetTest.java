package com.robaone.mylyn;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.robaone.api.data.AppDatabase;

public class XMLStorageTargetTest {
	ProjectInterface project;
	@Before
	public void setUp() throws Exception {
		project = new MylynProject();
		project.setName("Sample Project");
		project.setIdentifier("000001");
		project.setClient("100000");
		project.setOwner("junit");
		project.setStatus("Active");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToString() throws ParserConfigurationException, SAXException, IOException {
		XMLStorageTarget target = new XMLStorageTarget(project);
		String xml = target.toString();
		AppDatabase.writeLog(xml);
	}
	
	@Test
	public void testToStringWithTask() throws Exception {
		XMLStorageTarget target = new XMLStorageTarget(project);
		TaskInterface task = new MylynTask();
		task.setIdentifier("1");
		task.setName("Sample Task");
		task.setOwner("junit");
		task.setProjectId(project.getIdentifier());
		task.setTarget(target);
		TaskNoteInterface note = task.newNote();
		note.setStartDate(AppDatabase.getTimestamp());
		note.setEndDate(AppDatabase.getTimestamp());
		note.appendBody("this is a note");
		task.save(note);
		note = task.newNote();
		note.setStartDate(AppDatabase.getTimestamp());
		note.setEndDate(AppDatabase.getTimestamp());
		note.appendBody("this is another note");
		task.save(note);
		TaskInterface task2 = new MylynTask();
		task2.setIdentifier("2");
		task2.setName("Another Sample Task");
		task2.setOwner("junit");
		task2.setProjectId(project.getIdentifier());
		task2.setTarget(target);
		TaskNoteInterface note2 = task.newNote();
		note2.setStartDate(AppDatabase.getTimestamp());
		note2.setEndDate(AppDatabase.getTimestamp());
		note2.appendBody("this is a note for task2");
		task2.save(note2);
		note2 = task2.newNote();
		note2.setStartDate(AppDatabase.getTimestamp());
		note2.setEndDate(AppDatabase.getTimestamp());
		note2.appendBody("this is another note for task2");
		task2.save(note2);
		String xml = target.toString();
		AppDatabase.writeLog(xml);
	}

}
