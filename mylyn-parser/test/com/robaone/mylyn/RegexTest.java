package com.robaone.mylyn;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.robaone.api.data.AppDatabase;
import com.robaone.util.INIFileReader;

public class RegexTest {
	INIFileReader ini;
	@Test
	public void test() throws Exception {
		ini = new INIFileReader(this.getClass().getResourceAsStream("RegexTest.ini"));
		String pattern = ini.getValue("regex");
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(getText());
		if(m.find()){
			AppDatabase.writeLog("Matches = true");
			String found = getText().substring(m.start(), m.end());
			AppDatabase.writeLog(found);
			for(int i = 0; i < m.groupCount();i++){
				AppDatabase.writeLog("Group "+i+" = "+m.group(i));
			}
		}else{
			AppDatabase.writeLog("Matches = false");
		}
	}
	private String getText() throws IOException {
		String str = IOUtils.toString(this.getClass().getResourceAsStream("trello.json.txt"));
		return str;
	}

}
