package com.robaone.mylyn;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.Test;
import com.robaone.api.business.XMLDocumentReader;

public class MylynParserTest {
	XMLStorageTarget target = null;
	@Test
	public void testRun() throws Exception {
		InputStream tasksFile = this.getClass().getResourceAsStream("tasklist.xml");
		MylynParser parser = new MylynParser();
		target = new XMLStorageTarget();
		parser.setStorageTarget(target);
		parser.setTaskXML(tasksFile);
		parser.run();
		XMLDocumentReader reader = new XMLDocumentReader();
		reader.read(target.toString());
		assertEquals("9",reader.findXPathString("count(/projects/project)"));
		assertEquals("0006",reader.findXPathString("(/projects/project)[1]/identifier"));
		assertEquals("Oracle Migration",reader.findXPathString("/projects/project[identifier='0005']/name"));
		assertEquals("2",reader.findXPathString("count(/projects/project[identifier='0005']/tasks)"));
	}

	@Test
	public void testCreateProject1() {
		MylynParser parser = new MylynParser();
		MylynProject project = parser.createProject("Name", "Handle 1");
		assertNotNull(project);
		assertEquals("Name",project.getName());
		assertEquals("Handle 1",project.getIdentifier());
	}

	@Test
	public void testCreateProject2() {
		MylynParser parser = new MylynParser();
		MylynProject project = parser.createProject("001:Name", "Handle 1");
		assertNotNull(project);
		assertEquals("Name",project.getName());
		assertEquals("001",project.getIdentifier());
	}
}
