package com.robaone.scripts;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;

import com.robaone.mylyn.FindMylynTaskXML;
import com.robaone.mylyn.MylynParser;
import com.robaone.mylyn.XMLStorageTarget;
import com.robaone.xml.ROTransform;

public class ExportMylynTasks {


	public static void main(String[] args) {
		ExportMylynTasks exporter = new ExportMylynTasks();
		exporter.run(args);
	}

	protected void run(String[] args) {
		try{
			if(args.length < 2){
				throw new Exception("Not Enough Arguments");
			}else{
				if(args.length > 2){
					export(args[0],args[1],args[2]);
				}else{
					export(args[0],args[1]);
				}
			}
		}catch(Exception e){
			System.err.print("Error: ");
			System.err.println(e.getMessage());
			showUsage();
		}
	}
	public void export(String inputzip, String outputxml, String outputhtml) throws Exception {
		export(inputzip,outputxml);
		String outputHTMLDocument = createReportHTML(outputxml);
		save(outputhtml, outputHTMLDocument);
		String cssFile = getCssFile();
		File parentFolder = new File(outputhtml).getParentFile();
		save(parentFolder.getAbsolutePath()+System.getProperty("file.separator")+"project.css",cssFile);
		String csv = createTaskList(outputxml);
		final String csvFileName = parentFolder.getAbsolutePath()+System.getProperty("file.separator")+"tasklist.csv";
		save(csvFileName,csv);
		System.out.println("Saved to "+outputhtml);
		System.out.println("Saved to "+csvFileName);
	}

	protected String createTaskList(String outputxml) throws Exception {
		InputStream xsl1input = this.getClass().getResourceAsStream("tasklist.xsl");
		FileInputStream fin = new FileInputStream(new File(outputxml));
		ROTransform trn = new ROTransform(xsl1input);
		String outputXMLDocument = IOUtils.toString(fin);
		String out1 = trn.transformXML(outputXMLDocument);
		fin.close();
		xsl1input.close();
		
		InputStream xsl2input = this.getClass().getResourceAsStream("tasklist_to_csv.xsl");
		trn = new ROTransform(xsl2input);
		String outputCSVDocument = trn.transformXML(out1);
		
		return outputCSVDocument;
	}

	protected String getCssFile() throws IOException {
		InputStream in = this.getClass().getResourceAsStream("project.css");
		return IOUtils.toString(in);
	}

	protected String createReportHTML(String outputxml) throws Exception,
			IOException, FileNotFoundException {
		ROTransform trn = new ROTransform(this.getStylesheet());
		File outputXMLFile = new File(outputxml);
		final FileInputStream fileInputStream = new FileInputStream(outputXMLFile);
		String outputXMLDocument = IOUtils.toString(fileInputStream);
		String outputHTMLDocument = trn.transformXML(outputXMLDocument);
		fileInputStream.close();
		return outputHTMLDocument;
	}

	private void save(String filename, String contents)
			throws IOException {
		File output = new File(filename);
		FileWriter outputWriter = new FileWriter(output);
		outputWriter.write(contents);
		outputWriter.flush();
		outputWriter.close();
	}
	protected InputStream getStylesheet() {
		InputStream in = this.getClass().getResourceAsStream("projects.xsl");
		return in;
	}

	public void export(String inputzip, String outputxml) throws Exception {
		InputStream tasksFile = getTasksFile(inputzip);
		XMLStorageTarget target = parseTasks(tasksFile);
		saveOutput(outputxml, target);
	}

	private void saveOutput(String outputxml, XMLStorageTarget target)
			throws IOException {
		File output = new File(outputxml);
		FileWriter writer = new FileWriter(output);
		writer.write(target.toString());
		writer.flush();
		writer.close();
	}

	private XMLStorageTarget parseTasks(InputStream tasksFile)
			throws ParserConfigurationException, SAXException, IOException {
		MylynParser parser = new MylynParser();
		XMLStorageTarget target = new XMLStorageTarget();
		parser.setStorageTarget(target);
		parser.setTaskXML(tasksFile);
		parser.run();
		return target;
	}

	private InputStream getTasksFile(String inputzip)
			throws FileNotFoundException, Exception {
		File input = new File(inputzip);
		FindMylynTaskXML ziterator = new FindMylynTaskXML(input);
		ziterator.run();
		String xml = ziterator.getXmlfile();
		InputStream tasksFile = new ByteArrayInputStream(xml.getBytes());
		return tasksFile;
	}

	public void showUsage() {
		System.out.println("Usage: [input file] [output file]");
	}

}
