package com.robaone.mylyn;

public interface TaskInterface {

	public void setIdentifier(String id);
	
	public String getIdentifier();
	
	public void setOwner(String author);
	
	public String getOwner();
	
	public void setName(String title);
	
	public String getName();
	
	public void setProjectId(String projectId);
	
	public String getProjectId();
	
	public TaskNoteInterface newNote();

	public void save(TaskNoteInterface currentNote) throws Exception;

	void setTarget(StorageTargetInterface target);

	public int getNoteCount() throws Exception;
	
	public TaskNoteInterface getNote(int index) throws Exception;

}
