package com.robaone.mylyn;

public class MylynTaskFactory {

	public static MylynTask newInstance(String task_reference) {
		MylynTask task = new MylynTask();
		task.setIdentifier(task_reference);
		return task;
	}

}
