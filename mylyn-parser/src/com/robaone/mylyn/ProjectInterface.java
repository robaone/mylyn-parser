package com.robaone.mylyn;

import java.security.Timestamp;

public interface ProjectInterface {
	public String getName();
	public String getStatus();
	public String getOwner();
	public String getIdentifier();
	public Timestamp getDueDate();
	public String getClient();
	public void setName(String name);
	public void setStatus(String status);
	public void setOwner(String owner);
	public void setIdentifier(String identifier);
	public void setDueDate(Timestamp duedate);
	public void setClient(String client);
}
