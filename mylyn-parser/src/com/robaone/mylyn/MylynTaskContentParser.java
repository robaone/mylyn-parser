package com.robaone.mylyn;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.robaone.api.business.FieldValidator;
import com.robaone.api.data.AppDatabase;
import com.robaone.dbase.types.DateTimeType;
import com.robaone.util.LineReader;

public class MylynTaskContentParser implements Runnable {
	private String source;
	private InputStream sourceInputStream;
	private TaskInterface task;
	private Exception error;
	private TaskNoteInterface currentNote;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public TaskInterface getTask() {
		return task;
	}
	@Override
	public void run() {
		try {
			LineReader reader = new LineReader(this.getSourceInputStream());
			parse(reader);
		} catch (EOFException eof) {
			AppDatabase.writeTrace("End of File");
			try {
				this.save(currentNote);
			} catch (Exception e) {
				handleError(e);
			}
		} catch (Exception e) {
			handleError(e);
		}
	}
	private void handleError(Exception e) {
		e.printStackTrace();
		this.setError(e);
	}
	protected void parse(LineReader reader) throws Exception {
		currentNote = null;
		for(String line = reader.ReadLine();line != null;line = reader.ReadLine()){
			AppDatabase.writeTrace(line);
			if(isSectionHeader(line)){
				this.save(currentNote);
				currentNote = newSection(line);
			}else{
				try{
					if(currentNote != null){
						currentNote.appendBody(line);
					}else{
						System.out.println("Skipped: "+this.getTask().getName()+": "+line+"");
					}
				}catch(NullPointerException e){
					e.printStackTrace();
				}
			}
		}
	}
	protected void save(TaskNoteInterface currentNote) throws Exception {
		if(currentNote != null){
			this.getTask().save(currentNote);
		}
	}
	protected TaskNoteInterface newSection(String line) throws Exception {
		AppDatabase.writeTrace("newSection(\""+line+"\")");
		String[] split = line.split(":");
		String date = split[0].trim();
		String[] time = {"",""};
		if(split.length > 1){
			String[] timesplit = line.substring(split[0].length()+1).split("-");
			time[0] = timesplit[0].trim();
			if(timesplit.length > 1){
				time[1] = timesplit[1].trim();
			}
		}
		if(FieldValidator.exists(time[0]) && !FieldValidator.exists(time[1])){
			time[1] = time[0];
		}
		AppDatabase.writeTrace("Date = "+date);
		AppDatabase.writeTrace("Start Time = "+time[0]);
		AppDatabase.writeTrace("End Time   = "+time[1]);
		DateTimeType startDate = new DateTimeType(date + " " + time[0]);
		DateTimeType endDate = new DateTimeType(date + " " + time[1]);
		TaskNoteInterface newNote = this.getTask().newNote();
		newNote.setStartDate(startDate.getTimestamp());
		newNote.setEndDate(endDate.getTimestamp());
		if(newNote.getEndDate() != null && newNote.getStartDate() != null){
			if(newNote.getEndDate().before(newNote.getStartDate())){
				Calendar cal = Calendar.getInstance();
				cal.setTime(newNote.getEndDate());
				cal.add(Calendar.DATE, 1);
				newNote.setEndDate(new java.sql.Timestamp(cal.getTimeInMillis()));
			}
			if(newNote.getEndDate().getTime() - newNote.getStartDate().getTime() > 1000*60*60*12){
				Calendar cal = Calendar.getInstance();
				cal.setTime(newNote.getEndDate());
				cal.add(Calendar.HOUR, -12);
				newNote.setEndDate(new java.sql.Timestamp(cal.getTimeInMillis()));
			}
		}
		return newNote;
	}
	protected boolean isSectionHeader(String line) {
		if(line.trim().endsWith("-")){
			line = line.trim().substring(0,line.trim().length()-1);
		}
		Pattern p = Pattern.compile("^([0-9][0-9]?[/][0-9][0-9]?[/][0-9]{2}([0-9]{2})?)[:]([ ]*([0-9][0-9]?[:][0-9]{2}[ ]?[AaPp][Mm]?)([ ]*[-][ ]*([0-9][0-9]?[:][0-9]{2}[ ]?[AaPp][Mm]?)[ ]*)?)?$");
		Matcher m = p.matcher(line.trim());
		boolean matches = m.matches();
		return matches;
	}
	public void setSourceInputStream(InputStream in){
		this.sourceInputStream = in;
	}
	private InputStream getSourceInputStream() {
		if(this.sourceInputStream == null){
			this.sourceInputStream = new ByteArrayInputStream(this.getSource().getBytes());
		}
		return this.sourceInputStream;
	}
	public void setTask(TaskInterface storage) {
		this.task = storage;
	}
	public Exception getError() {
		return error;
	}
	public void setError(Exception error) {
		this.error = error;
	}

}
