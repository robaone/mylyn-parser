package com.robaone.mylyn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.zip.ZipEntry;

import org.apache.commons.io.IOUtils;

import com.robaone.util.ZipIterator;

public class FindMylynTaskXML extends ZipIterator {
	private String xmlfile;
	public FindMylynTaskXML(File file) throws FileNotFoundException {
		super(file);
	}

	@Override
	protected void process(ZipEntry entry) throws Exception {
		this.setXmlfile(IOUtils.toString(this.getEntryInputStream()));
	}

	@Override
	protected boolean excluded(ZipEntry entry) throws Exception {
		boolean retval = true;
		if(entry.getName().equalsIgnoreCase("tasklist.xml")){
			retval = false;
		}
		return retval;
	}

	public String getXmlfile() {
		return xmlfile;
	}

	protected void setXmlfile(String xmlfile) {
		this.xmlfile = xmlfile;
	}
}
