package com.robaone.mylyn;

public class MylynTask implements TaskInterface {
	private StorageTargetInterface target;
	private String id;
	private String title;
	private String projectId;
	private String author;
	private java.util.Date duedate;
	private String status;
	private String href;
	private java.util.Date enddate;
	private double hours;
	private int estimatedhours;
	protected StorageTargetInterface getTarget() {
		return target;
	}
	@Override
	public void setTarget(StorageTargetInterface target) {
		this.target = target;
	}

	@Override
	public TaskNoteInterface newNote() {
		return new MylynNote();
	}

	@Override
	public void save(TaskNoteInterface currentNote) throws Exception {
		target.save(currentNote,this);
	}

	@Override
	public void setIdentifier(String id) {
		this.id = id;
	}

	@Override
	public String getIdentifier() {
		return this.id;
	}

	@Override
	public void setOwner(String author) {
		this.author = author;
	}

	@Override
	public String getOwner() {
		return this.author;
	}

	@Override
	public void setName(String title) {
		this.title = title;
	}

	@Override
	public String getName() {
		return this.title;
	}

	@Override
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	@Override
	public String getProjectId() {
		return this.projectId;
	}
	@Override
	public int getNoteCount() throws Exception {
		return this.getTarget().getNoteCount(this);
	}
	@Override
	public TaskNoteInterface getNote(int index) throws Exception {
		return this.getTarget().getNote(index,this);
	}
	@Override
	public boolean equals(Object other){
		if(this.getIdentifier() != null && other instanceof TaskInterface){
			if(this.getIdentifier().equals(((TaskInterface)other).getIdentifier())){
				return true;
			}
		}
		return false;
	}
	@Override
	public int hashCode(){
		return this.getIdentifier().hashCode();
	}
	public java.util.Date getDuedate() {
		return duedate;
	}
	public void setDuedate(java.util.Date duedate) {
		this.duedate = duedate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public int getEstimatedhours() {
		return estimatedhours;
	}
	public void setEstimatedhours(int estimatedhours) {
		this.estimatedhours = estimatedhours;
	}
	public java.util.Date getEnddate() {
		return enddate;
	}
	public void setEnddate(java.util.Date enddate) {
		this.enddate = enddate;
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hours) {
		this.hours = hours;
	}
}
