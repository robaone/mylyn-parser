package com.robaone.mylyn.trello;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import com.robaone.api.business.FieldValidator;

/**
 * Attach a file to a Trello Card
 * @author ansel
 *
 */
public class TrelloAttachFile implements Runnable {
	private HttpClient httpclient;
	private HttpPost httpPost;
	private String filename;
	private FileBody fileBody;
	private MultipartEntity entity;
	private HttpResponse response;
	private Exception exception;
	private String name;
	private String cardid;
	private TrelloCredentials credentials;
	private String url;


	@Override
	public void run() {
		try {
			this.getEntity().addPart("file",this.getFileBody());
			this.getEntity().addPart("name",new StringBody(this.getName(),"text/plain",Charset.forName("UTF-8")));
			this.getEntity().addPart("key",new StringBody(this.getCredentials().getKey(),"text/plain",Charset.forName("UTF-8")));
			this.getEntity().addPart("token",new StringBody(this.getCredentials().getToken(),"text/plain",Charset.forName("UTF-8")));
			//this.getEntity().addPart("url",new StringBody("http://log.roboane.com/files/thisisafile.txt","text/plain",Charset.forName("UTF-8")));
			this.getHttpPost().setEntity(getEntity());
			this.setResponse(this.getHttpclient().execute(this.getHttpPost()));
		} catch (Exception e) {
			this.setException(e);
		}
	}

	public HttpClient getHttpclient() {
		if(httpclient == null){
			httpclient = new DefaultHttpClient();
		}
		return httpclient;
	}

	public void setHttpclient(HttpClient httpclient) {
		this.httpclient = httpclient;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public FileBody getFileBody() {
		if(fileBody == null){
			fileBody = new FileBody(new File(this.getFilename()));
		}
		return fileBody;
	}

	public void setFileBody(FileBody fileBody) {
		this.fileBody = fileBody;
	}

	public MultipartEntity getEntity() {
		if(this.entity == null){
			this.entity = new MultipartEntity();
		}
		return entity;
	}

	public void setEntity(MultipartEntity entity) {
		this.entity = entity;
	}


	public String getUrl() {
		if(url == null){
			url = "https://trello.com/1/cards/"+this.getCardid()+"/attachments";
		}
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HttpPost getHttpPost() {
		if(this.httpPost == null){
			this.httpPost = new HttpPost(this.getUrl());
		}
		return httpPost;
	}

	public void setHttpPost(HttpPost httpPost) {
		this.httpPost = httpPost;
	}

	public HttpResponse getResponse() {
		return response;
	}

	public void setResponse(HttpResponse response) {
		this.response = response;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TrelloCredentials getCredentials() {
		return credentials;
	}

	public void setCredentials(TrelloCredentials credentials) {
		this.credentials = credentials;
	}

	public String getCardid() {
		return cardid;
	}

	public void setCardid(String cardid) {
		this.cardid = cardid;
	}

}
