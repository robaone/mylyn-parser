package com.robaone.mylyn.trello;

public class TrelloCredentials {
	private String key;
	private String token;
	public void setKey(String key){
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public void setToken(String token){
		this.token = token;
	}
	public String getToken() {
		return this.token;
	}

}
