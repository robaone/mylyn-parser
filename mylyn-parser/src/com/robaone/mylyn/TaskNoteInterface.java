package com.robaone.mylyn;

import java.sql.Timestamp;

public interface TaskNoteInterface {

	void setStartDate(Timestamp timestamp);

	void setEndDate(Timestamp timestamp);

	void appendBody(String line);

	Timestamp getStartDate();

	Timestamp getEndDate();
	
	double getHours();

	String getBody();

}
