package com.robaone.mylyn;

import java.security.Timestamp;

public class MylynProject implements ProjectInterface {
	private String name;
	private String status;
	private String owner;
	private String identifier;
	private Timestamp dueDate;
	private String client;
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getStatus() {
		return this.status;
	}

	@Override
	public String getOwner() {
		return this.owner;
	}

	@Override
	public String getIdentifier() {
		return this.identifier;
	}

	@Override
	public Timestamp getDueDate() {
		return this.dueDate;
	}

	@Override
	public String getClient() {
		return this.client;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public void setDueDate(Timestamp duedate) {
		this.dueDate = duedate;
	}

	@Override
	public void setClient(String client) {
		this.client = client;
	}

}
