package com.robaone.mylyn;

import java.sql.Timestamp;
import java.util.Calendar;

public class MylynNote implements TaskNoteInterface {
	private Timestamp startDate;
	private Timestamp endDate;
	private StringBuffer body = new StringBuffer();
	@Override
	public void setStartDate(Timestamp timestamp) {
		this.startDate = timestamp;
	}
	
	@Override
	public Timestamp getStartDate(){
		return this.startDate;
	}

	@Override
	public void setEndDate(Timestamp timestamp) {
		this.endDate = timestamp;
	}
	
	@Override
	public Timestamp getEndDate(){
		return this.endDate;
	}

	@Override
	public void appendBody(String line) {
		if(body == null){
			body = new StringBuffer();
		}
		if(body.length() > 0){
			body.append("\n");
		}
		body.append(line);
	}
	
	@Override
	public String getBody() {
		return this.body.toString();
	}
	
	public double getHours() {
		double hours = 0.0;
		if(this.getStartDate() != null && this.getEndDate() != null){
			long duration = this.getEndDate().getTime() - this.getStartDate().getTime();
			hours = (double)duration/(1000*60*60);
		}
		return hours;
	}

}
