package com.robaone.mylyn;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.robaone.api.business.FieldValidator;
import com.robaone.api.business.XMLDocumentReader;

public class MylynParser implements Runnable {
	private InputStream taskXML;
	private XMLDocumentReader reader;
	private Exception exception;
	private StorageTargetInterface storageTarget;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S Z");
	@Override
	public void run() {
		try {
			reader = new XMLDocumentReader();
			reader.read(this.getTaskXML());
			parse();
		} catch (Exception e) {
			this.setException(e);
		}
	}
	protected void parse() throws Exception {
		NodeList categories = reader.findXPathNode("//*[name()='TaskCategory' or name()='Query']");
		for(int i = 0; i < categories.getLength();i++){
			parseCategory(categories.item(i));
		}
	}
	protected void parseCategory(Node category_node) throws Exception {
		NodeList handle_nodes = reader.findXPathNode(category_node,"./*[name()='TaskReference' or name()='QueryHit']");
		String project_name = reader.findXPathString(category_node,"@Name");
		String project_handle = reader.findXPathString(category_node,"@Handle");
		MylynProject project = this.createProject(project_name,project_handle);
		save(project);
		for(int i = 0; i < handle_nodes.getLength();i++){
			parseProjectHandle(project,handle_nodes.item(i));
		}
	}
	protected void save(MylynProject project) throws Exception {
		StorageTargetInterface storage = this.getStorageTarget();
		storage.save(project);
	}
	protected MylynProject createProject(String project_name,String project_handle) {
		MylynProject project = MylynProjectFactory.newProject(project_name,project_handle);
		return project;
	}
	protected void parseProjectHandle(MylynProject project, Node task_item) throws Exception {
		String task_reference = reader.findXPathString(task_item,"@Handle");
		NodeList tasks = reader.findXPathNode("//Task[@Handle='"+task_reference+"']");
		for(int i = 0; i < tasks.getLength();i++){
			parseProjectTask(project,tasks.item(i),task_reference);
		}
	}
	protected void parseProjectTask(MylynProject project, Node task_node, String task_reference) throws Exception {
		MylynTask task = this.createTask(task_reference);
		task.setTarget(getStorageTarget());
		String label = reader.findXPathString(task_node,"@Label");
		String taskid = reader.findXPathString(task_node,"@TaskId");
		String priority = reader.findXPathString(task_node,"@Priority");
		String notes = reader.findXPathString(task_node,"@Notes");
		String active = reader.findXPathString(task_node,"@Active");
		String htmlreference = reader.findXPathString(task_node,"@IssueURL");
		String estimatedhours = reader.findXPathString(task_node,"@Estimated");
		String duedate = reader.findXPathString(task_node,"@DueDate");
		String enddate = reader.findXPathString(task_node,"@EndDate");
		String scheduledenddate = reader.findXPathString(task_node,"@ScheduledEndDate");
		task.setName(label);
		task.setIdentifier(taskid);
		task.setDuedate(FieldValidator.exists(duedate) ? toDate(duedate) : (FieldValidator.exists(scheduledenddate) ? toDate(scheduledenddate) : null));
		task.setStatus(FieldValidator.exists(active) ? (("false".equalsIgnoreCase(active) && FieldValidator.exists(enddate)) ? "Done" : "Active") : "Active");
		task.setEnddate(FieldValidator.exists(enddate) ? toDate(enddate) : null);
		task.setHref(htmlreference);
		task.setEstimatedhours(FieldValidator.isNumber(estimatedhours) ? new Integer(estimatedhours) : 0);
		task.setProjectId(project.getIdentifier());
		
		parseTaskNotes(task,notes);
		
	}
	protected void parseTaskNotes(MylynTask task,String notes) {
		MylynTaskContentParser parser = new MylynTaskContentParser();
		parser.setSource(notes);
		parser.setTask(task);
		parser.run();
	}
	protected java.sql.Date toDate(String duedate) throws ParseException {
		java.util.Date date = dateFormatter.parse(duedate);
		return new java.sql.Date(date.getTime());
	}
	protected MylynTask createTask(String task_reference) {
		MylynTask task = MylynTaskFactory.newInstance(task_reference);
		return task;
	}
	public InputStream getTaskXML() {
		return taskXML;
	}
	public void setTaskXML(InputStream taskXML) {
		this.taskXML = taskXML;
	}
	public Exception getException() {
		return exception;
	}
	private void setException(Exception exception) {
		this.exception = exception;
	}
	public StorageTargetInterface getStorageTarget() {
		return storageTarget;
	}
	public void setStorageTarget(StorageTargetInterface storageTarget) {
		this.storageTarget = storageTarget;
	}

}
