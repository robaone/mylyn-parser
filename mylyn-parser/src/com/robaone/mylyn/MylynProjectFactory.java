package com.robaone.mylyn;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MylynProjectFactory {

	public static MylynProject newProject(String project_name,String project_handle) {
		MylynProject project = new MylynProject();
		Pattern p = Pattern.compile("([0-9]+):(.+)");
		Matcher m = p.matcher(project_name);
		if(m.matches()){
			String identifier = m.group(1);
			String name = m.group(2);
			project.setName(name.trim());
			project.setIdentifier(identifier);
		}else{
			project.setName(project_name);
			project.setIdentifier(project_handle);
		}
		return project;
	}

}
