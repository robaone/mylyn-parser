package com.robaone.mylyn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.robaone.api.business.XMLDocumentReader;

public class XMLStorageTarget implements StorageTargetInterface {
	private XMLDocumentReader reader;
	private HashMap<String,ProjectInterface> projects = new HashMap<String,ProjectInterface>();
	private HashMap<String,HashMap<String,TaskInterface>> project_tasks = new HashMap<String,HashMap<String,TaskInterface>>();
	private HashMap<String,ArrayList<TaskNoteInterface>> task_notes = new HashMap<String,ArrayList<TaskNoteInterface>>();
	public XMLStorageTarget() throws ParserConfigurationException, SAXException, IOException{
		reader = new XMLDocumentReader();
	}
	public XMLStorageTarget(ProjectInterface project) throws ParserConfigurationException, SAXException, IOException{
		reader = new XMLDocumentReader();
		this.save(project);
	}
	@Override
	public void save(TaskNoteInterface currentNote, TaskInterface task)
			throws Exception {
		HashMap<String,TaskInterface> tasks = this.project_tasks.get(task.getProjectId());
		if(tasks == null){
			tasks = new HashMap<String,TaskInterface>();
			this.project_tasks.put(task.getProjectId(), tasks);
		}
		tasks.put(task.getIdentifier(),task);
		ArrayList<TaskNoteInterface> notes = task_notes.get(task.getIdentifier());
		if(notes == null){
			notes = new ArrayList<TaskNoteInterface>();
			task_notes.put(task.getIdentifier(), notes);
		}
		notes.add(currentNote);
	}

	@Override
	public int getNoteCount(TaskInterface mylynTask) throws Exception {
		ArrayList<TaskNoteInterface> notes = task_notes.get(mylynTask);
		return notes.size();
	}

	@Override
	public TaskNoteInterface getNote(int index, TaskInterface mylynTask)
			throws Exception {
		ArrayList<TaskNoteInterface> notes = task_notes.get(mylynTask);
		return notes.get(index);
	}

	public Document toDocument() throws Exception{
		reader.read(getXML());
		return reader.getDocument();
	}

	protected String getXML() throws Exception {
		JSONObject jo = new JSONObject();
		String[] identifiers = this.projects.keySet().toArray(new String[0]);
		JSONArray projects_ja = new JSONArray();
		jo.put("project", projects_ja);
		for(int index = 0; index < this.projects.size();index++){
			ProjectInterface project = this.projects.get(identifiers[index]);
			projects_ja.put(new JSONObject(project));
			addTasksToXML(projects_ja.getJSONObject(projects_ja.length()-1),project.getIdentifier());
		}
		return XML.toString(jo,"projects");
	}
	private void addTasksToXML(JSONObject object, String identifier) throws Exception {
		HashMap<String, TaskInterface> tasks = this.project_tasks.get(identifier);
		if(tasks != null){
			String[] taskIdentifiers = tasks.keySet().toArray(new String[0]);
			JSONArray ja = new JSONArray();
			for(int i = 0; i < taskIdentifiers.length;i++){
				String taskId = taskIdentifiers[i];
				TaskInterface task = tasks.get(taskId);
				JSONObject jo = new JSONObject(task);
				this.addTaskNotesToXML(jo,this.task_notes.get(taskId));
				ja.put(jo);
			}
			object.put("tasks", ja);
		}
	}
	private void addTaskNotesToXML(JSONObject jo,
			ArrayList<TaskNoteInterface> arrayList) throws Exception {
		JSONArray ja = new JSONArray();
		for(int i = 0; i < arrayList.size();i++){
			TaskNoteInterface note = arrayList.get(i);
			ja.put(new JSONObject(note));
		}
		jo.put("notes", ja);
	}
	@Override
	public String toString(){
		try {
			return this.getXML();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public HashMap<String,ProjectInterface> getProjects() {
		return projects;
	}
	public void setProjects(HashMap<String,ProjectInterface> projects) {
		this.projects = projects;
	}
	@Override
	public void save(ProjectInterface project) {
		this.projects.put(project.getIdentifier(), project);
	}
}
