package com.robaone.mylyn;

public interface StorageTargetInterface {
	void save(ProjectInterface project);

	void save(TaskNoteInterface currentNote, TaskInterface mylynStorage) throws Exception;

	int getNoteCount(TaskInterface mylynTask) throws Exception;

	TaskNoteInterface getNote(int index, TaskInterface mylynTask) throws Exception;

}
